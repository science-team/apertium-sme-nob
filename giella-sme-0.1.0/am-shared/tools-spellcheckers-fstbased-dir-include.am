## Process this file with automake to produce Makefile.in

## Copyright (C) 2011 Samediggi

## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.

## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Hfst fst variables:
GT_SPELLER_SRC_HFST=generator-speller-gt-norm.hfst
GT_COMMON_SPELLER_HFST=

####### Automake targets: ########

if WANT_SPELLERS
if CAN_HFST
GT_COMMON_SPELLER_HFST+=generator-fstspeller-gt-norm.hfst
GT_COMMON_SPELLER_HFST+=analyser-fstspeller-gt-norm.hfst
endif # CAN_HFST
endif # WANT_SPELLERS

noinst_DATA=$(GT_COMMON_SPELLER_HFST)

#### Build the speller transducer with compound filtering:
generator-fstspeller-gt-norm.tmp.hfst: \
			../$(GT_SPELLER_SRC_HFST) \
			$(top_builddir)/src/filters/remove-hyphenation-marks.hfst    \
			$(top_builddir)/src/filters/remove-infl_deriv-borders.hfst
	$(AM_V_HXFST)$(PRINTF) "read regex                                        \
			@\"$<\"                                                           \
		.o. @\"$(top_builddir)/src/filters/remove-hyphenation-marks.hfst\"    \
		.o. @\"$(top_builddir)/src/filters/remove-infl_deriv-borders.hfst\"   \
		; \n\
		 twosided flag-diacritics\n\
		 save stack $@\n\
		 quit\n" | $(HFST_XFST) -p $(MORE_VERBOSITY) $(HFST_FORMAT)


# Copy the tmp transducer to the final one. This allows local overrides.
%.hfst: %.tmp.hfst
	$(AM_V_CP)cp -f $< $@

# Invert the final fst, to enable symmetric yaml tests and easy manual testing:
analyser-%.hfst: generator-%.hfst
	$(AM_V_INVERT)$(HFST_INVERT) $(MORE_VERBOSITY) $(HFST_FLAGS) -i $< -o $@

####### Other targets: ###########
clean-local:
	-rm -f *.hfst *.txt *.att

include $(top_srcdir)/am-shared/xfscript-include.am
include $(top_srcdir)/am-shared/hfst-format-include.am
include $(top_srcdir)/am-shared/silent_build-include.am
